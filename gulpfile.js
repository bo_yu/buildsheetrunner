global.gulp = global.gulp || require('gulp');

var exec = require('child_process').exec;
var execProcess = function(command, options, callback) {
    if(!options.maxBuffer) {
        options.maxBuffer = 1024 * 50000;
    }
    var process = exec(command, {maxBuffer: 1024 * 50000});

    process.stdout.on('data', function(data) {

       // req.pipe(request('http://mysite.com/doodle.png')).pipe(resp);

        console.log(data);
    });

    process.stderr.on('data', function(data) {
        err = true;
        console.log(data);
    });

    if (typeof callback === 'function') {
        process.on('exit', function() {
            if (!err) {
                return callback();
            }
        });
    }
};


gulp.task('default', function(done) {
    execProcess('ant -f ../zelda/common/deploy.xml', function (err) {
        console.log(err)
        done();
    });
});