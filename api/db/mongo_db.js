var express = require('express');
var router = express.Router({mergeParams: true});
var mongoose = require('mongoose');
var logger = rootRequire("/utils/logger");

router.use(function (req, res, next) {
    console.log(req.originalUrl, ' Time: ', Date.now());
    next();
});

// define the home parent level page route
router.get('/api/action*', function(req, res) {
    res.send('unit action home page');
});

router.get('/api/action/find', function(reg, res) {
    var Kitten = mongoose.model('kittens'); // mongoose.model('Kitten', kittySchema);
    Kitten.find(function (err, kittens) {
        if (err) return console.error(err);
        res.send(kittens);
    });
})
module.exports = function(app) {
    app.use(router);
}