var express = require('express');
var fs= require('fs');
var async = require('async');
var _ = require("lodash");
var util = require('util');
var path = require('path');
var router = express.Router({mergeParams: true});
//var gulp = global.gulp  = require('gulp');
var exec = require('child_process').exec;
var child = rootRequire('/utils/process');

var logger = rootRequire('/utils/logger');
var PARENT_PATH="/api/deploy/action";


router.get(PARENT_PATH + '/gulp', function(req, res) {
    gulp.start('default');
});

module.exports = function(app) {
    app.use(router);
}