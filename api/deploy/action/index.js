var express = require('express');
var fs= require('fs');
var async = require('async');
var _ = require("lodash");
var util = require('util');
var path = require('path');
var router = express.Router({mergeParams: true});
//var gulp = global.gulp  = require('gulp');
var exec = require('child_process').exec;
var child = rootRequire('/utils/process');

var logger = rootRequire('/utils/logger');
var PARENT_PATH="/api/deploy/action";

//parent level path handler
router.use(PARENT_PATH +'*', function (req, res, next) {
    logger.info(req.originalUrl + '  parent level2 called');
    next();
});

module.exports = function(app) {
    app.use(router);
}