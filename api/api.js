var express = require('express');
var router = express.Router();
var logger = rootRequire('/utils/logger');

//parent path path handler
router.all('/api*', function(req, res, next) {
    logger.info(req.originalUrl,' parent level called - maybe use to check security first');
    next();
});

module.exports = function(app) {
    app.use(router);
}

