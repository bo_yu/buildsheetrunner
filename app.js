//var gulp = global.gulp  = require('gulp');
//require('./gulpfile.js');

var expressValidator = require('express-validator');
var path           = require('path');
var express        = require('express');
var morgan         = require('morgan');
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');
var app            = express();
var cors = require('cors');

global.rootRequire = function(name) {
    return require(path.join(__dirname, name));
}
var logger         = rootRequire("/utils/logger");
app.use(cors());
app.use(express.static(__dirname + '/public'));     // set the static files location /public/img will be /img for users
app.use(bodyParser.urlencoded({ extended: false}));    // parse application/x-www-form-urlencoded
//app.use(bodyParser({limit: '50mb'}));
app.use(bodyParser.json({limit: '50mb'}));    // parse application/json

app.use(expressValidator());
app.use(methodOverride());                  // simulate DELETE and PUT


var utils = rootRequire('/utils/loader');
utils.loaddirs(path.join(__dirname,'api'), app);
utils.loaddirs(path.join(__dirname,'models'));

/**************************************************
var dbURI= 'mongodb://localhost/action';
var mongoose = require('mongoose');
var db = mongoose.connect(dbURI);

mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open to ' + dbURI);
});

// If the connection throws an error
mongoose.connection.on('error',function (err) {
    console.log('Mongoose default connection error: ' + err);
});

mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});
******************************************/
// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function() {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});

// catch the uncaught errors that weren't wrapped in a domain or try catch statement
// do not use this in modules, but only in applications, as otherwise we could have multiple of these bound
process.on('uncaughtException', function(err) {
    // handle the error safely
    console.log(err)
})

global.appRoot = path.resolve(__dirname);
app.listen(8081);
logger.info('Magic happens on port 8081');          // shoutout to the user



//interaction
//gulp.start('default');