/*
 Contains logic used by billing screen dialogs
 also see billing.js

 Author: Al Wolfson
 Date October, 2012
 */

'use strict';

function tableUpdate() {
    $(".dbTable").trigger("update");
}

function dialogRowAdd(that, edit) {
    var row = [],
        rowFields = [],
        billingType = that.attr('ACCOUNTBILLINGGROUPID'),
        startDate = $('#startDate').val(),
        endDate = $('#endDate').val(),
        program = (that.attr('PROGRAMID') === undefined) ? 0 : that.attr('PROGRAMID'),
        campaign = (that.attr('CAMPAIGNID') === undefined) ? 0 : that.attr('CAMPAIGNID'),
        servicecode = (that.attr('SERVICECODE') === undefined) ? "" : that.attr('SERVICECODE'),
        rateid = (that.attr('RATEID') === undefined) ? 0 : that.attr('RATEID'),
        changed = 'false',
        len = 0,
        i = 0,
        val = '';

    //blank out end date for account level rates
    if( $('#Dialog #labelId').val() == 0) {
        endDate = '';
    }

    that.removeClass('highlight');

    rowFields = ['t_name','t_campaign','t_type','t_ratetype','t_language','t_PERMEMBERRATE','t_PERMINUTERATE','t_PERMINUTETRANSFERRATE','t_MINIMUMCALLDUR','t_CALLDURINCREMENT','t_TIMINGMODECODE','t_startdate','t_enddate'];

    if(!edit) {
        rateid = 0;
        changed = 'true';
    }

    row.push('<tr ACCOUNTBILLINGGROUPID="' + billingType + '" RATEID="' + rateid + '" CAMPAIGNID="' + campaign + '" PROGRAMID="' + program + '" SERVICECODE="' + servicecode + '" CHANGED="' + changed + '">');

    len = rowFields.length;
    for (i = 0; i < len; i++) {
        if(edit || ( rowFields[i] != 't_startdate' && rowFields[i] != 't_enddate')) {
            val = that.children('td.'+rowFields[i]).html();
            val = (val === null) ? '' : val;
            row.push('<td class="' + rowFields[i] + '">' + val + '</td>');
        }
    }

    if(!edit) {
        row.push('<td class="t_startdate">'+startDate+'</td>');
        row.push('<td class="t_enddate">'+endDate+'</td>');
    }

    row.push('<td class="controls"></td></tr>');
    $('#ProgramTo tbody').append(row.join(''));

    editSingleRow($('#ProgramTo tbody tr:last'));
    tableUpdate();
}

function dialogRowRemove(row) {
    if(row.attr('rateid') != 0) {
        deleteSingleRate(row, false);
    } else {
        row.remove();
    }
    tableUpdate();
}


function validateLabelForm() {
    var valid=true,
        val='',
        namefld=$('#Name'),
        startfld=$('#Start_Date'),
        endfld=$('#End_Date'),
        patt=/(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d/;

    val=namefld.val();
    if (val == '') {
        namefld.parent().append(inlineErrorMsg('Please enter a Name')).addClass('ui-state-error');
        valid=false;
    }else if (val.length > 50) {
        namefld.parent().append(inlineErrorMsg('Names have a mximum length of 50')).addClass('ui-state-error');
        valid=false;
    }

    val=startfld.val();
    if (val == '') {
        startfld.parent().append(inlineErrorMsg('Please enter a Start Date')).addClass('ui-state-error');
        valid=false;
    }else if (! patt.test(val)) {
        startfld.parent().append(inlineErrorMsg('Please specify a valid date - mm/dd/yyyy')).addClass('ui-state-error');
        valid=false;
    }

    val=endfld.val();
    if (val == '') {
        endfld.parent().append(inlineErrorMsg('Please enter an End Date')).addClass('ui-state-error');
        valid=false;
    }else if (! patt.test(val)) {
        endfld.parent().append(inlineErrorMsg('Please specify a valid date - mm/dd/yyyy')).addClass('ui-state-error');
        valid=false;
    }

    return valid;
}

function updateValidRates() {
//move any valid rates to billing group (ST-11107)
    var labelId = $('#labelId').val(),
        validrows = $('#ProgramTo tbody tr.valid');

    if (validrows.length){
        validrows.each( function() {
            $('#label'+labelId+' table tbody').append($(this));
        });
        $('#label'+labelId+' table tbody tr.valid').each( function() {
            printSingleRow($(this));
        });

        $('#label'+labelId+' table tbody tr.placeholder').remove();

        initDragDrop();
    }
}

function showDialog(title, height, width, okDisabled) {
    $("#Dialog").dialog({
        resizable: true,
        modal: true,
        width: width,
        height: height,
        buttons: [
            {
                id: "button-ok",
                text: "Ok",
                classes: "ui-priority-primary",
                click: function() {
                    if($('#dialogForm').hasClass('labelForm')) {
                        if(validateLabelForm()) {
                            updateLabel();
                        }
                    } else {
                        updateRates();
                    }
                    //updateTableSorter();
                }
            },
            {
                id: "button-cancel",
                text: "Cancel",
                classes: "ui-priority-secondary",
                click: function() {
                    if($('#dialogForm').hasClass('labelForm')) {
                        //label form
                    } else {
                        //rate form
                        updateValidRates();
                    }
                    $(this).dialog("close");
                }
            }
        ],
        open : function() {
            $('#Dialog').dialog( "option" , "title" ,title);
            if(okDisabled) {
                $("#button-ok").button("disable").addClass( 'ui-state-disabled' );
            }
            $('.ui-dialog-titlebar').append('<a href="#" class="ui-dialog-titlebar-close ui-corner-all" style="margin-right:30px" role="button" id="maximizeDialog"><span class="ui-icon ui-icon-squaresmall-plus">maximize</span></a>');
            $("#Dialog .dbTable").tablesorter();
        }

    });
}

//dialog submit functions

//save or edit billing group
function updateLabel() {
    $("#Description").htmlarea("updateTextArea");
    var values = $('.labelForm').serialize(),
        labelId = $('.labelForm #labelId').val(),
        startDate = $('.labelForm #Start_date').val(),
        endDate = $('.labelForm #End_Date').val(),
        cloneId = $('.labelForm #clone').val(),
        tbl = '';

    $.post('processor.cfm?proctype=labelModify',values, function(data) {
        var obj = data[0];

        if(obj.error_ind === '0') {
            if(labelId != 0) {
                tbl = $('#label'+labelId).find('tbody');
                $('#label'+labelId).replaceWith(obj.html);
                $('#label'+labelId).find('tbody').html(tbl.html());
                init('#label'+labelId);
                showNotification('Billing Group Updated','Changes saved', false);
            } else {
                $('.billingLabels').append(obj.html);

                showNotification('Billing Group Added','Changes saved', false);
                labelId=obj.accountbillinglabelid;
            }
            stylePortlets($('#label'+labelId));
            if (processDate(endDate) < new Date()) {
                $('#label'+labelId).addClass('past');
                if(!$('#hidePast').attr('checked')) {
                    $('#label'+labelId).show();
                }
            }
            $('#Dialog').dialog( "close" );

            //check to see if this is a clone operation
            if(cloneId != '') {

                tbl = $('#label'+cloneId).find('table');

                $("#Dialog").load('rate_form.cfm?accountId='+accountId, function() {

                    //disable type select if this is a global label group
                    if(cloneId == 0) {
                        $('#billingType').attr('disabled', true);
                    }
                    $('#Dialog #labelId').val(labelId);
                    $('#Dialog #startDate').val(startDate);
                    $('#Dialog #endDate').val(endDate);

                    //copy existing rates, update dates to new label dates (using edit = false for dialogrowadd)
                    tbl.children('tbody').children('tr').each(function() {
                        if(!$(this).hasClass('placeholder')) {
                            dialogRowAdd($(this), false);
                        }
                    });

                    init('#Dialog');
                    showDialog('Adjust Rates', 720, 950, false);
                });
            } else {
                initDragDrop();
            }

        } else {
            showNotification('Unable to update Billing Group', obj.error_message, true);
        }
        //$('#dbg').html(data);
    },"json");

}

/*
 save rates from dialog
 Since ajax runs asynchronously, we use a counter to associate with each request, and can then update the related row by referencing the nth-child
 Must loop through all rows to ensure that the counter is accurately tied to the correct table row
 to correctly check for gaps and overlaps, we need to sort the rates
 by start date. To maintain the same visible order we need to do the following:
 -	Assign a row counter
 -	Copy rows to a tmp table
 -	Sort tmp table on start date
 -	Save row by row
 -	Assign errors back to original table
 -	Dispose of tmp table
 */
function updateRates() {
    var rateId = 0,
        labelId = $('#labelId').val(),
        programId = 0,
        campaignId = '',
        serviceCode = 0,
        success = true,
        rows = $('#ProgramTo tbody tr'),
        rowCount = rows.length,
        counter = 0,
        tmprows = '';

    //early exit, nothing has changed
    if(rowCount == $('#ProgramTo tbody tr[changed="false"]').length) {
        $( '#Dialog' ).dialog( "close" );
        return;
    }

    //create tmp table
    $("<table id='tmpTable' style='display:none'><tbody/></table>").appendTo('body');

    //assign counters, check for input errors
    rows.each(function() {
        var that=$(this);

        //remove previous error states and selections
        that.removeClass('ui-state-error valid highlight')
            .attr('COUNTER',++counter)
            .find('.errmsg').remove();

        if(!validateRateRow(that)) {
            success=false;
        } else {
            //explicitly set values since clone loses user seelcted values/options
            that.find('input:text').each(function() {
                $(this).attr('value', $(this).val());
            });
            $('#tmpTable tbody').append(that.clone());

            var selectMenus = that.find('select');

            $('#tmpTable tbody tr:last').find('select').each(function(i) {
                /* See this: */
                $('option', this)[selectMenus[i].selectedIndex].selected = true;
            });

        }
    });

    //stop processing if we have an invalid row
    if(!success) {
        //remove tmpTable
        $('#tmpTable').remove();
        return;
    }

    tmprows = $('#tmpTable tbody tr');

    //sort tmp table by start date ascending
    tmprows.sort(row_newest_first_sort).appendTo('#tmpTable tbody');

    tmprows.each(function() {
        var that = $(this),
            counter = that.attr('COUNTER'),
            values = '',
            formType = $('#Dialog #formType').val();

        //only process any items that have changed
        if(that.attr('changed') == 'true') {
            that.children('td').each(function() {
                $(this).find('input,select').attr('disabled', false);
            });

            //check if campaign or program....
            rateId = $(this).attr('rateId');
            programId = $(this).attr('programId');
            campaignId = $(this).attr('campaignId');
            serviceCode = $(this).attr('servicecode');

            values = $(this).serializeAnything()+'&rateId='+rateId+'&accountId='+accountId+'&programId='+programId+'&campaignId='+campaignId+'&labelId='+labelId+'&serviceCode='+serviceCode;

            $.ajax({
                type: "GET",
                dataType: 'json',
                url: 'processor.cfm?proctype=rateModify&counter='+(counter),
                data: values,
                async: false,
                success: function(data) {
                    var row=$('#ProgramTo tbody tr:nth-child('+data[0].counter+')'),
                        first = '';

                    if(data[0].error_ind == '0') {
                        row.addClass('valid')
                            .attr('rateid',data[0].accountrateid).attr('changed','false');
                    } else {
                        row.addClass('ui-state-error');
                        first = row.find('td:first');
                        first.html(inlineErrorMsg(data[0].error_message)+first.html());
                        success=false;
                    }

                }
            }).promise().done(function() {
                if(counter == rowCount) {
                    if(success) {
                        if(formType == 'editBillingGroup') {
                            $('#ProgramTo tbody tr').each(function() {
                                printSingleRow($(this));
                            });
                            $('#label'+labelId+' table tbody').html($('#ProgramTo tbody').html());
                        } else {
                            updateValidRates();
                        }
                        initDragDrop();
                        $('#Dialog').dialog( "close" );

                        showNotification('Success','Changes have been saved', false);
                    } else {
                        showNotification('Error(s) detected','Please correct the indicated errors and then press OK', true);
                    }
                }
                //remove tmpTable
                $('#tmpTable').remove();
            });
        } else {
            that.addClass('valid');
        }
    });
}

function maximizeDialog() {
    $("#Dialog").dialog('option','width','100%')
        .dialog('option', 'position', [0,0]);
}

function dialogInit() {
    var DELAY = 200,
        clicks = 0,
        timer = null;

    $('#Dialog .dbTable tbody tr').live("click", function(e) {
        var that = $(this),
            tblId = that.parentsUntil('table').parent().attr('id');
        clicks++;
        if(clicks == 1) {
            timer = setTimeout(function() {

                if (e.ctrlKey) {
                    that.addClass('highlight');
                } else {
                    that.addClass('highlight').siblings().removeClass('highlight');
                }
                clicks = 0;             //after action performed, reset counter
            }, DELAY);
        } else if(tblId != 'ProgramTo') {
            //don't allow double click in ProgramTo - otherwise it screws up form field manipulation
            clearTimeout(timer);    //prevent single-click action
            dialogRowAdd(that, false);

            clicks = 0;             //after action performed, reset counter
        }
    })
        .live("dblclick", function(e) {
            e.preventDefault();  //cancel system double-click event
        });


    $('#ProgramFrom td').disableSelection();

    $("#tableFilter").live("keyup", function() {
        var filter = $(this).val();
        $("#ProgramFrom tbody tr").each(function() {
            var match = $(this).text().search(new RegExp(filter, "i"));
            if (match < 0)  {
                $(this).addClass('filtered');
            }else {
                $(this).removeClass('filtered');
            }
        });
    });
    $('.ui-dialog-titlebar').live("dblclick", function() {
        maximizeDialog();
    });

    $('#maximizeDialog').live("click", function() {
        maximizeDialog();
    });
}


function printDialogFromTable(data,len,currentType) {
    var table = [],
        groupId = 2,
        campaign = '',
        campaignId = '',
        obj = '',
        i = 0;

    if(currentType == -2){
        groupId = 3;
    } else if(currentType<0) {
        groupId = 1;
    } else if(currentType > 0) {
        groupId = 3;
        campaign = $('#billingType option:selected').text();
        campaignId = $('#billingType option:selected').val();
    }

    table.push('<table class="dbTable ui-corner-all ui-widget ui-widget-content" id="ProgramFrom"><thead>');
    table.push('<th>Name</th>');
    table.push('<th>Campaign</th>');
    table.push('<th>Type</th>');
    table.push('<th>Category</th>');
    table.push('<th>Start Date</th>');
    table.push('<th>Program Status</th>');
    table.push('</thead><tbody>');
    if(len) {
        if(currentType == -2) {
            for (i = 0; i < len; i++) {
                obj = data[i];

                table.push('<tr programid="" campaignid="' + obj.campaignid + '" ACCOUNTBILLINGGROUPID="' + groupId + '" INTERVENTIONMODECODE=""><td class="t_name">'  + obj.campaignid + ' - ' + obj.campaignname + '</td>');
                table.push('<td class="t_campaign"></td>');
                table.push('<td class="t_type">Campaign</td>');
                table.push('<td>' + obj.categoryname + '</td>');
                table.push('<td>' + obj.effectivedatetime.substring(0,obj.effectivedatetime.length -8) + '</td>');
                table.push('<td>' + obj.campaignstatus + '</td></tr>');
            }
        } else {
            for (i = 0; i < len; i++) {
                obj = data[i];
                table.push('<tr programid="' + obj.programid + '" campaignid="' + campaignId + '" ACCOUNTBILLINGGROUPID="' + groupId + '" INTERVENTIONMODECODE="' + obj.interventionmodecode + '"><td class="t_name">' + obj.programid + ' - ' + obj.programname + '</td>');
                table.push('<td class="t_campaign">' + campaign + '</td>');
                table.push('<td class="t_type">' + obj.interventionmodecode + '</td>');
                table.push('<td>' + obj.categoryname + '</td>');
                table.push('<td>' + obj.programstartdatetime.substring(0,obj.programstartdatetime.length -8) + '</td>');
                table.push('<td>' + obj.programstatus + '</td></tr>');
            }
        }
    }
    table.push('</tbody></table>');

    $('#ProgramFrom').replaceWith(table.join(''));
}

function onChangeBillingGroup() {
    var currentType = $('#billingType option:selected').val(),
        CampaignId = -1,
        len = 0;

    switch (currentType) {
        //individual campaigns
        case '-2':
            break;

        //account level
        case '-1':
            break;

        //individual programs
        case '0':
            CampaignId=0;
            break;

        //campaign specific programs
        default:
            CampaignId=currentType;
            break;

    }
    if(currentType == -2) {
        $.get('processor.cfm?proctype=campaignSelect&accountId='+accountId, function(data) {
            len = data.length;
            printDialogFromTable(data,len,currentType);
            $("#ProgramFrom").tablesorter();
            tableUpdate();
        },"json");
    }else if(CampaignId >=0) {
        $.get('processor.cfm?proctype=programSelect&accountId='+accountId+'&CampaignId='+CampaignId, function(data) {
            len = data.length;
            printDialogFromTable(data,len,currentType);
            $("#ProgramFrom").tablesorter();
            tableUpdate();
        },"json");
    } else {
        $.get('processor.cfm?proctype=printAccountRateTable&accountId='+accountId, function(data) {
            $('#ProgramFrom').replaceWith(data);
            $("#ProgramFrom").tablesorter();
            tableUpdate();
        });
    }
}

function toggleDialogColumn(columnIndex) {
    $('#ProgramTo td:nth-child(' + (columnIndex + 1) + '),#ProgramTo th:nth-child(' + (columnIndex + 1) + ')').toggle();
}