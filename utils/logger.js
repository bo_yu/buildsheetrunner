var fs = require('fs'),
    path = require('path'),
    winston = require('winston');

var filename = path.join(__dirname,'../logs', 'created-logfile.log');

var twoDigit = '2-digit';
var options = {
    day: twoDigit,
    month: twoDigit,
    year: twoDigit,
    hour: twoDigit,
    minute: twoDigit,
    second: twoDigit
};
function formatter(args) {
    var dateTimeComponents = new Date().toLocaleTimeString('en-us', options).split(',');
    var logMessage = dateTimeComponents[0] + dateTimeComponents[1] + ' - ' + args.level + ': ' + args.message;
    return logMessage;
}

var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            formatter: formatter,
            handleExceptions: true,
            colorize: true
        }),
        new (winston.transports.File)({
            filename: filename,
            formatter: formatter,
            handleExceptions: true,
            colorize: true
        })
    ]
});

logger.log('info', 'Hello created log files!',filename, {'foo': 'bar'});
module.exports = logger;
