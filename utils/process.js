var exec = require('child_process').exec;

var execProcess = function(res, command, options, callback) {
    var process = exec(command, {maxBuffer: 1024 * 50000});
    process.stdout.pipe(res);
    process.stderr.pipe(res);

    if (typeof callback === 'function') {
        process.on('exit', function() {
            return callback(err);
        });
    }
};

module.exports = {
    exec: execProcess
}