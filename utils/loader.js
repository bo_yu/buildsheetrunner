var path = require('path');
var fs = require('fs');

var loadAll = function (dir, app) {
    fs.readdirSync(dir).forEach(function (folder) {
        var myPath = path.join(dir, folder);
        if (fs.lstatSync(myPath).isDirectory()) {
            loadAll(myPath, app);
        } else {
            if(app) {
                require(myPath)(app);
            } else {
                require(myPath);
            }

        }
    });
}


module.exports = {
    loaddirs: loadAll
}

